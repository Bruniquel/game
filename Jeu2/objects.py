import pygame
import button
import drawing
from drawing import font,colors,screen_height,bottom_panel,screen

start_inventory_number = 150
start_inventory = 100
#button images
heal_img = pygame.image.load('img/Icons/potion.png').convert_alpha()
strength_img = pygame.image.load('img/Icons/potion2.png').convert_alpha()
#create buttons
heal_button = button.Button(screen, 20, screen_height - bottom_panel + 70, heal_img, 64, 64,"heal 15 hp")
strength_button = button.Button(screen, 20, screen_height - bottom_panel + 70, strength_img, 64, 64," give 10 AD for 2 turns")


class Objects():
    def __init__(self,cases,nombre,effet,effect_color = None,button=None,duration=0):
        self.cases = cases
        self.nombre = nombre
        self.effet = effet
        self.duration = duration
        self.start = 0
        self.button = button
        self.effect_color = effect_color

  
def health_potions(nombre, health_amount,button=None):
    return Objects(False,nombre,[health_amount,0],colors['green'],button)

def strenght_potions(nombre, strenght_amount,button=None,duration=0):
    return Objects(False,nombre,[0,strenght_amount],colors['yellow'],button,duration)

def check_inventory(objects):
    inventory_number_index = start_inventory_number
    inventory_index = start_inventory
    for obj in objects:
        if obj.button != None:
            obj.button.rect.x = inventory_index
            if obj.button.draw():
                obj.cases = True
            drawing.draw_text(str(obj.nombre), font, colors['red'], inventory_number_index, screen_height - bottom_panel + 90)
            inventory_number_index += 80     
            inventory_index += 80


