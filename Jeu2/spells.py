import pygame
bottom_panel =250
screen_width = 800
screen_height = 400 + bottom_panel

screen = pygame.display.set_mode((screen_width, screen_height))

class Spell():
    def __init__(self, name, x, y,damage, mana_cost):
        self.name = name
        self.x = x
        self.y = y
        self.damage = damage
        self.mana_cost = mana_cost
        self.animation_list = []
        self.frame_index = 0
        self.action = 0#0:idle, 1:attack, 2:hurt, 3:dead
        self.update_time = pygame.time.get_ticks()
        #load idle images
        temp_list = []
        for i in range(8):
            img = pygame.image.load(f'img/{self.name}/{i}.png')
            temp_list.append(img)
        self.animation_list.append(temp_list)
        self.image = self.animation_list[self.action][self.frame_index]
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.action = 1

    def update(self):
            animation_cooldown = 80
            #handle animation
            #update image
            self.image = self.animation_list[self.action][self.frame_index]
            #check if enough time has passed since the last update
            if pygame.time.get_ticks() - self.update_time > animation_cooldown:
                self.update_time = pygame.time.get_ticks()
                self.frame_index += 1
                
                
            #if the animation has run out then reset back to the start
            if self.frame_index >= len(self.animation_list[self.action]):
                if self.action == 3:
                    self.frame_index = len(self.animation_list[self.action]) - 1

                else:
                    self.idle()
    def idle(self):
            #set variables to idle animation
            self.action = 1
            self.frame_index = 0
            self.update_time = pygame.time.get_ticks()

    def draw(self):
        if self.action == 0:
            self.update()
            screen.blit(self.image, self.rect)
        #draw the damage text

#q image
q_syndra = Spell("q_syndra",590,305,15,10)
