import pygame



pygame.init()
#define fonts
font = pygame.font.SysFont('Times New Roman', 20)
font_small = pygame.font.SysFont('Times New Roman', 15)

bottom_panel =250
screen_width = 800
screen_height = 400 + bottom_panel

screen = pygame.display.set_mode((screen_width, screen_height))


#define colours
colors = {
	'red' : (255, 0, 0),
	'green' : (0, 255, 0),
	'blue' : (0,0,255),
	'yellow' : (255,211,67),
	'black' : (0,0,0)
}


#load images
text_panel = pygame.image.load('img/Icons/text_panel.png').convert_alpha()
zhonya = pygame.image.load('img/Icons/zhonya.png').convert_alpha()
#background image
background_img = pygame.image.load('img/Background/background.png').convert_alpha()
#panel image
panel_img = pygame.image.load('img/Icons/panel.png').convert_alpha()
panel_vide = pygame.image.load('img/Icons/panel_vide.png').convert_alpha()
inventory_img = pygame.image.load('img/Icons/inventory.png').convert_alpha()

#load victory and defeat images
victory_img = pygame.image.load('img/Icons/victory.png').convert_alpha()
defeat_img = pygame.image.load('img/Icons/defeat.png').convert_alpha()



#function for drawing background
def draw_bg():
	screen.blit(background_img, (0, 0))

#create function for drawing text
def draw_text(text, font, text_col, x, y):
	if len(text)>28:
		img = font.render(text[:28], True, text_col)
		screen.blit(img, (x, y))
		draw_text(text[28:], font, text_col, x, y+20)
	else:
		img = font.render(text, True, text_col)
		screen.blit(img, (x, y))
#function for drawing panel
def draw_panel(player,bandits):
	#draw panel rectangle	
	panel = pygame.transform.scale(panel_img, (screen_width,bottom_panel))
	inventory = pygame.transform.scale(inventory_img, (300,154))
	empty_item = pygame.transform.scale(panel_vide,(64,64))
	screen.blit(panel, (0, screen_height - bottom_panel))
	screen.blit(inventory, (60, screen_height - bottom_panel +80))
	screen.blit(zhonya,(100,  screen_height - bottom_panel + 160))
	screen.blit(empty_item,(180,  screen_height - bottom_panel + 160))
	screen.blit(empty_item,(260,  screen_height - bottom_panel + 160))
	#show knight stats
	draw_text(f'{player.name} HP: {player.hp}', font, colors['red'], player.health_bar.x, player.health_bar.y-30)
	draw_text(f'MANA: {player.mana}', font, colors['blue'], player.mana_bar.x, player.mana_bar.y-30)
	draw_text(f'Strength: {player.strength}', font_small, colors['yellow'], 30, screen_height - bottom_panel- 20)
	for count, i in enumerate(bandits):
		#show name and health
		draw_text(f'{i.name} HP: {i.hp}', font, colors['red'], 550, 378 + 40*i.emplacement)


def draw_objects_texts(buttons_list,pos):
	for button in buttons_list:
		if len(button.text) > 0:
			if button.rect.collidepoint(pos):
				screen.blit(text_panel,[530,20])
				draw_text(button.text, font_small ,colors['black'],550,40)
				


def drawAll(player,bandits):
	#draw background
	draw_bg()

	#draw panel
	draw_panel(player,bandits)
	player.health_bar.draw(player.hp)
	player.mana_bar.draw(player.mana)
	for bandit in bandits:
		bandit.health_bar.draw(bandit.hp)
	#draw fighters
	player.update()
	player.draw()
	for bandit in bandits:
		bandit.update()
		bandit.draw()
	
def gameOver(res):
     #check if game is over
	if res != 0:
		if res == 1:
			screen.blit(victory_img, (250, 50))
		if res == -1:
			screen.blit(defeat_img, (290, 50))

class DamageText(pygame.sprite.Sprite):
    def __init__(self, x, y, damage, colour):
        pygame.sprite.Sprite.__init__(self)
        self.image = font.render(damage, True, colour)
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.counter = 0
        
    def update(self):
		#move damage text up
        self.rect.y -= 2
		#delete the text after a few seconds
        self.counter += 2
        if self.counter > 30:
            self.kill()
            
class Bar():
	def __init__(self, x, y, stat, max_stat,color,size):
		self.x = x
		self.y = y
		self.stat = stat
		self.max_stat = max_stat
		self.color = color
		self.size = size


	def draw(self, stat):
		#update with new health
		self.stat = stat
		#calculate health ratio
		ratio = self.stat / self.max_stat
		pygame.draw.rect(screen, colors['red'], (self.x, self.y, self.size, 20))
		pygame.draw.rect(screen, self.color, (self.x, self.y, self.size * ratio, 20))

