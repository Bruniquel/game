import pygame
import pytmx
import pyscroll


class Entity(pygame.sprite.Sprite):
    
    def __init__(self, name, x ,y):
        
        pygame.sprite.Sprite.__init__(self)
        self.sprite_sheet = pygame.image.load(f"sprite\{name}.png")
        self.image = self.get_image(0,0)
        self.image.set_colorkey([0,0,0])
        self.rect = self.image.get_rect()
        self.position = [x,y]
        self.images = {
            'down': self.get_image(0,0),
            'left': self.get_image(0,32),
            'right': self.get_image(0,64),
            'up': self.get_image(0,96)
        }
        self.feet = pygame.Rect(0,0,self.rect.width * 0.5, 12)
        self.old_position = self.position.copy()
        self.speed = 3
        self.not_talking = True
        
    def save_location(self): self.old_position = self.position.copy()
    
    def change_animation(self, name): 
        self.image = self.images[name]
        self.image.set_colorkey((0,0,0))
    
    def move_right(self): 
        if(self.not_talking):
            self.change_animation("right")
            self.position[0] += self.speed
    
    def move_left(self): 
        if(self.not_talking):
            self.change_animation("left")
            self.position[0] -= self.speed
    
    def move_up(self):
        if(self.not_talking):
            self.change_animation("up")
            self.position[1] -= self.speed
    
    def move_down(self):
        if(self.not_talking):
            self.change_animation("down")
            self.position[1] += self.speed
    
    def update(self):
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom
    
    def move_back(self):
        self.position = self.old_position
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom
        
    def get_image(self, x, y):
        image = pygame.Surface([32,32])
        image.blit(self.sprite_sheet,(0,0),(x,y,32,32))
        return image
    
class Player(Entity):
    
    def __init__(self):
        super().__init__("yasuo", 0, 0)
        
class NPC(Entity):
    
    def __init__(self,name,nb_points,dialog, condition = -1):
        super().__init__(name,0,0)
        self.nb_points = nb_points
        self.points = []
        self.name = name
        self.dialog = dialog
        self.speed = 1
        self.current_point = 0
        self.condition = condition
        
    def move(self):
        current_point = self.current_point
        target_point = self.current_point+1
        
        if target_point >= self.nb_points:
            target_point = 0
        
        current_rect = self.points[current_point]
        target_rect = self.points[target_point]
        
        if current_rect.y < target_rect.y and abs(current_rect.x - target_rect.x) < 3:
            self.move_down()
        
        elif current_rect.y > target_rect.y and abs(current_rect.x - target_rect.x) < 3:
            self.move_up()
            
        elif current_rect.x > target_rect.x and abs(current_rect.y - target_rect.y) < 3:
            self.move_left()
        
        elif current_rect.x < target_rect.x and abs(current_rect.y - target_rect.y) < 3:
            self.move_right()
        
        if self.rect.colliderect(target_rect):
            self.current_point = target_point
        
    def teleport_spawn(self):
        location = self.points[self.current_point]
        self.position[0] = location.x
        self.position[1] = location.y
        self.save_location()

        
    def load_points(self, map):
        for num in range(1, self.nb_points+1):
            point = map.get_object_by_name(f"{self.name}_path{num}")
            rect = pygame.Rect(point.x, point.y, point.width, point.height)
            self.points.append(rect)


class HealthBar():
    def __init__(self,x,y,hp,max_hp):
        self.x = x
        self.y = y
        self.hp = hp
        self.max_hp = max_hp
        
    def draw(self,hp):
        pygame.draw.rect(screen, red, (self.x, self.y, 150,20))