from re import S
from turtle import update
import pygame
import random
import button
from spells import q_syndra
from button import list_buttons, lvls_buttons
from fighter import Fighter

import drawing

from drawing import colors,screen,font,font_small

from levels import ennemies

import objects


pygame.init()

clock = pygame.time.Clock()
fps = 60

bottom_panel = 250
screen_width = 800
screen_height = 400 + bottom_panel
pygame.display.set_caption('RogueLike-Lol Version')


#define game variables
current_fighter = 1
action_cooldown = 0
action_wait_time = 50
attack = False
potion = False
potion_effect = 15
clicked = False
game_over = 0




e_img = pygame.image.load('img/e_syndra/0.png').convert_alpha()
#map image
map_image = pygame.image.load('img/Icons/map.png').convert_alpha()

#sword image
sword_img = pygame.image.load('img/Icons/sword.png').convert_alpha()


player = Fighter(200, 220, 'Syndra', 30, 10,0,30,[objects.health_potions(6,15,list_buttons['heal']),objects.strenght_potions(2,10,list_buttons['strength'],2),objects.strenght_potions(8,10,list_buttons['strength2']
,2)])

object_list = list_buttons.values()



index_level = 0
tour = 1


run = True
while run:

	clock.tick(fps)

	drawing.drawAll(player,ennemies[index_level])
	q_syndra.draw()
	attack = False
	target = None
	
		
	
	#control player actions
	#reset action variables
	
	
	#make sure mouse is visible
	pygame.mouse.set_visible(True)
	pos = pygame.mouse.get_pos()
	
	for count, bandit in enumerate(ennemies[index_level]):
		if bandit.rect.collidepoint(pos):
			#hide mouse
			pygame.mouse.set_visible(False)
			#show sword in place of mouse cursor
			screen.blit(sword_img, pos)
			if clicked == True and bandit.alive == True:
				attack = True
				target = ennemies[index_level][count]

	if player.cast_spell(target):
		attack = False
		current_fighter += 1
		action_cooldown = 0
	
	drawing.draw_objects_texts(object_list,pos)
	objects.check_inventory(player.objects)

	if game_over == 0:
		#player action
		if player.alive == True:
			if current_fighter == 1:
				action_cooldown += 1
				if action_cooldown >= action_wait_time:
					#look for player action
					#attack
					if attack == True and target != None:
						player.attack(target)
						current_fighter += 1
						action_cooldown = 0
					#potion
					effects=player.activate_objects(tour)
					for effect in effects:
						if effect > 0:
							current_fighter += 1
							action_cooldown = 0
							break

		else:
			game_over = -1


		#enemy action
		for count, bandit in enumerate(ennemies[index_level]):
			if current_fighter == 2 + count:
				if bandit.alive == True:
					action_cooldown += 1
					if action_cooldown >= action_wait_time:
						#check if bandit needs to heal first
						if (bandit.hp / bandit.max_hp) < 0.5:
							if bandit.objects[0].nombre > 0:
								bandit.objects[0].cases = True
							else:
								bandit.attack(player)
								current_fighter += 1
								action_cooldown = 0

							effects = bandit.activate_objects(tour)
							#check if the potion would heal the bandit beyond max health
							for effect in effects:
								if effect > 0:
									current_fighter += 1
									action_cooldown = 0
									break

						#attack
						else:
							bandit.attack(player)
							current_fighter += 1
							action_cooldown = 0
				else:
					current_fighter += 1

		#if all fighters have had a turn then reset
		if current_fighter > len(ennemies[index_level])+1:
			tour += 1
			current_fighter = 1


	#check if all bandits are dead
	alive_bandits = 0
	for bandit in ennemies[index_level]:
		if bandit.alive == True:
			alive_bandits += 1
	if alive_bandits == 0:
		game_over = 1


	#check if game is over
	if game_over != 0:
		tour = 1
		drawing.gameOver
		if game_over == 1:
			if list_buttons['next_lvl'].draw():
				if len(ennemies)-1 > index_level:
					index_level += 1
					player.reset()
					for bandit in ennemies[index_level]:
						bandit.reset()
					current_fighter = 1
					action_cooldown
					game_over = 0
		if list_buttons['restart'].draw():
			index_level = 0
			player.reset()
			for bandit in ennemies[index_level]:
				bandit.reset()
			current_fighter = 1
			action_cooldown
			game_over = 0
		if list_buttons['map'].draw():
			if list_buttons['map'].show:
				list_buttons['map'].show = False
			else:
				list_buttons['map'].show = True
				
	if (list_buttons['map'].show):
		screen.blit(map_image,[180,140])
		for count, lvl in enumerate(lvls_buttons):
			if lvl.draw():
				list_buttons['map'].show = False
				index_level = count
				player.reset()
				for bandit in ennemies[index_level]:
					bandit.reset()
				current_fighter = 1
				action_cooldown
				game_over = 0

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			run = False
		if event.type == pygame.MOUSEBUTTONDOWN:
			clicked = True
		else:
			clicked = False

	pygame.display.update()

pygame.quit()

