import pygame
import random
import button
import spells

from drawing import DamageText
from drawing import screen
from drawing import Bar,colors
from spells import q_syndra
from button import list_buttons


#define colours
red = (255, 0, 0)

green = (0, 255, 0)



damage_text_group = pygame.sprite.Group()
#fighter class
class Fighter():
	def __init__(self, x, y, name, max_hp, strength,emplacement,max_mana,objects=[]):
		self.name = name
		self.max_hp = max_hp
		self.hp = max_hp
		self.mana = max_mana
		self.max_mana = max_mana
		self.strength = strength
		self.start_strength = 0 + strength
		self.objects = objects
		self.start_objects = []
		self.x = x
		self.y = y
		for obj in self.objects:
			self.start_objects.append(obj.nombre)
		self.alive = True
		self.animation_list = []
		self.frame_index = 0
		self.action = 0#0:idle, 1:attack, 2:hurt, 3:dead
		self.update_time = pygame.time.get_ticks()
		self.emplacement = emplacement
		if self.emplacement == 0:
			self.health_bar = Bar(60, 450, self.hp, self.max_hp,colors['green'],150)
			self.mana_bar = Bar(250,450,self.mana,self.max_mana,colors['blue'],110)
		elif self.emplacement > 0:
			self.health_bar = Bar(550, 400 + 40*self.emplacement, self.hp, self.max_hp,colors['green'],150)
			self.mana_bar = None
        
		#load idle images
		temp_list = []
		for i in range(8):
			img = pygame.image.load(f'img/{self.name}/Idle/{i}.png')
			img = pygame.transform.scale(img, (img.get_width() * 3, img.get_height() * 3))
			temp_list.append(img)
		self.animation_list.append(temp_list)
		#load attack images
		temp_list = []
		for i in range(8):
			img = pygame.image.load(f'img/{self.name}/Attack/{i}.png')
			img = pygame.transform.scale(img, (img.get_width() * 3, img.get_height() * 3))
			temp_list.append(img)
		self.animation_list.append(temp_list)
		#load hurt images
		temp_list = []
		for i in range(3):
			img = pygame.image.load(f'img/{self.name}/Hurt/{i}.png')
			img = pygame.transform.scale(img, (img.get_width() * 3, img.get_height() * 3))
			temp_list.append(img)
		self.animation_list.append(temp_list)
		#load death images
		temp_list = []
		for i in range(10):
			img = pygame.image.load(f'img/{self.name}/Death/{i}.png')
			img = pygame.transform.scale(img, (img.get_width() * 3, img.get_height() * 3))
			temp_list.append(img)
		self.animation_list.append(temp_list)
		self.image = self.animation_list[self.action][self.frame_index]
		self.rect = self.image.get_rect()
		self.rect.center = (x, y)


	def update(self):
		animation_cooldown = 100
		#handle animation
		#update image
		damage_text_group.draw(screen)
		self.image = self.animation_list[self.action][self.frame_index]
		#check if enough time has passed since the last update
		if pygame.time.get_ticks() - self.update_time > animation_cooldown:
			self.update_time = pygame.time.get_ticks()
			self.frame_index += 1
			damage_text_group.update()
			
		#if the animation has run out then reset back to the start
		if self.frame_index >= len(self.animation_list[self.action]):
			if self.action == 3:
				self.frame_index = len(self.animation_list[self.action]) - 1
			else:
				self.idle()


	
	def idle(self):
		#set variables to idle animation
		self.action = 0
		self.frame_index = 0
		self.update_time = pygame.time.get_ticks()

	def activate_objects(self,tour):
		effects = [0,0]
		for count, obj in enumerate(self.objects):
			if obj.start > 0:
				if (tour-obj.start) > obj.duration:
					print(obj.effet[1])
					self.strength -= obj.effet[1]
					obj.start = 0
			if obj.cases == True:
				if obj.nombre > 0:
					if count == 0:
						if self.hp == self.max_hp: obj.nombre += 1

						if self.max_hp - self.hp > obj.effet[0]:
							self.hp += obj.effet[0]
							effects[0]+= obj.effet[0]
						else :
							effects[0]+=(self.max_hp-self.hp)
							self.hp += self.max_hp-self.hp
							
					self.strength += obj.effet[1]
					effects[1]+=obj.effet[1]
					obj.nombre -= 1
					if obj.duration > 0:
						obj.start = tour
					obj.cases = False
					for effect in effects:
						if effect>0:
							damage_text = DamageText(self.rect.centerx, self.rect.y, "+"+str(effect), obj.effect_color)
							damage_text_group.add(damage_text)

		return effects


	def attack(self, target):
		#deal damage to enemy
		rand = random.randint(-5, 5)
		damage = self.strength + rand
		target.hp -= damage
		#run enemy hurt animation
		target.hurt()
		#check if target has died
		if target.hp < 1:
			target.hp = 0
			target.alive = False
			target.death()
		damage_text = DamageText(target.rect.centerx, target.rect.y, "-"+str(damage), red)
		damage_text_group.add(damage_text)
		#set variables to attack animation
		self.action = 1
		self.frame_index = 0
		self.update_time = pygame.time.get_ticks()
	
	def cast_spell(self, target=None):
		casted = False
		list_buttons['e'].draw()
		if list_buttons['q'].show:
			list_buttons['q'].draw()
			if target != None:
				if self.mana >= q_syndra.mana_cost:
					q_syndra.rect.center = (target.x, target.y+30)
					q_syndra.action = 0
					target.hp -= q_syndra.damage
					if target.hp < 1:
						target.hp = 0
						target.alive = False
						target.death()
					damage_text = DamageText(target.rect.centerx, target.rect.y, "-"+str(q_syndra.damage), red)
					damage_text_group.add(damage_text)
					self.mana -= q_syndra.mana_cost
					casted = True
				list_buttons['q'].show = False
				
			pygame.draw.rect(screen,colors['red'],pygame.Rect(list_buttons['q'].x,list_buttons['q'].y,45,45),3)
		
		if not(list_buttons['q'].show):
			if list_buttons['q'].draw():
				if self.mana >= q_syndra.mana_cost:
					list_buttons['q'].show = True
	
		return casted

	def hurt(self):
		#set variables to hurt animation
		self.action = 2
		self.frame_index = 0
		self.update_time = pygame.time.get_ticks()

	def death(self):
		#set variables to death animation
		self.action = 3
		self.frame_index = 0
		self.update_time = pygame.time.get_ticks()


	def reset (self):
		self.alive = True
		for count, obj in enumerate(self.objects):
			obj.nombre = self.start_objects[count]
			obj.start = 0
		self.strength = self.start_strength
		self.mana = self.max_mana
		self.hp = self.max_hp
		self.frame_index = 0
		self.action = 0
		self.update_time = pygame.time.get_ticks()
  
	def draw(self):
		screen.blit(self.image, self.rect)
	    #draw the damage text
		

  