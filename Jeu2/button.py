import pygame 
from drawing import screen,screen_height,bottom_panel,colors

#button class
class Button():
	def __init__(self, surface, x, y, image, size_x, size_y,text=''):
		self.image = pygame.transform.scale(image, (size_x, size_y))
		self.rect = self.image.get_rect()
		self.rect.topleft = (x, y)
		self.clicked = False
		self.surface = surface
		self.show = True
		self.text = text
		self.x = x
		self.y = y

	def draw(self):
		action = False
		#get mouse position
		pos = pygame.mouse.get_pos()	
		#check mouseover and clicked conditions
		if self.rect.collidepoint(pos):
			if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
				action = True
				self.clicked = True

		if pygame.mouse.get_pressed()[0] == 0:
			self.clicked = False

		#draw button
		self.surface.blit(self.image, (self.rect.x, self.rect.y))

		return action

#button images
heal_img = pygame.image.load('img/Icons/potion.png').convert_alpha()
strength_img = pygame.image.load('img/Icons/potion2.png').convert_alpha()
map_button_img = pygame.image.load('img/Icons/map_button.png').convert_alpha()
restart_img = pygame.image.load('img/Icons/restart.png').convert_alpha()
next_level_img = pygame.image.load('img/Icons/next_lv.png').convert_alpha()
q_img = pygame.image.load('img/q_syndra/q.png').convert_alpha()
e_img = pygame.image.load('img/e_syndra/e.png').convert_alpha()

lvls = []
for i in range(4):
	lvls.append(pygame.image.load(f'img/Icons/lv{i+1}.png').convert_alpha())
#create buttons
heal_button = Button(screen, 100, screen_height - bottom_panel + 90, heal_img, 64, 64,"heal 15 hp")
strength_button = Button(screen, 180, screen_height - bottom_panel + 90, strength_img, 64, 64," give 10 AD for 2 turns")
restart_button = Button(screen, 330, 120, restart_img, 120, 30)
next_level_button = Button(screen, 330, 180, next_level_img, 120, 30)
map_button = Button(screen, 350,80, map_button_img, 80,50)
strength_button2 = Button(screen, 200, screen_height - bottom_panel + 90, strength_img, 64, 64,"give 10 AD for 2 turns")
q_button = Button(screen, 150, 350, q_img, 45,45,"Dark Sphere. Syndra conjures a Dark Sphere dealing magic damage.")
q_button.show = False
e_button = Button(screen, 200, 350, e_img, 45,45,"Syndra repousse les ennemis et les sphères noires.")
e_button.show = False
map_button.show = False
lvls_buttons = []
emplacements_lvls = [[290,235],[430,275],[520,220],[615,340]]
for count, lvl in enumerate(lvls):
	lvls_buttons.append(Button(screen,emplacements_lvls[count][0],emplacements_lvls[count][1],lvl,32,32))

list_buttons = {
	'heal' : heal_button,
	'strength' : strength_button,
	'restart' : restart_button,
	'next_lvl' : next_level_button,
	'map' : map_button,
	'strength2' : strength_button2,
	'q' : q_button,
	'e' : e_button
}