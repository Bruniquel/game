import pygame
from fighter import Fighter
from objects import Objects
import objects



ennemies = []

#lv1
bandit_list_lv1 = []
bandit_list_lv1.append(Fighter(600, 270, 'Bandit', 10, 6, 1,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
ennemies.append(bandit_list_lv1) 

#lv2
bandit_list_lv2 = []
bandit_list_lv2.append(Fighter(660, 270, 'Bandit', 15, 6, 1,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
bandit_list_lv2.append(Fighter(570, 290, 'Bandit', 15, 6, 2,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
ennemies.append(bandit_list_lv2)

#lv3
bandit_list_lv3 = []
bandit_list_lv3.append(Fighter(660, 270, 'Bandit', 20, 6, 1,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
bandit_list_lv3.append(Fighter(600, 290, 'Bandit', 20, 6, 2,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
ennemies.append(bandit_list_lv3)

#lv4
bandit_list_lv4 = []
bandit_list_lv4.append(Fighter(660, 270, 'Bandit', 20, 6, 1,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
bandit_list_lv4.append(Fighter(570, 290, 'Bandit', 10, 6, 2,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
bandit_list_lv4.append(Fighter(700, 320, 'Bandit', 10, 6, 3,0,[objects.health_potions(1,10),objects.strenght_potions(0,10,1)]))
ennemies.append(bandit_list_lv4)