from dataclasses import dataclass
from player import NPC
from player import HealthBar
import pygame, pytmx, pyscroll

@dataclass
class Portal:
    from_world: str
    origin_point: str
    condition: int
    target_world: str
    teleport_point: str

@dataclass
class Map:
    name: str
    walls: list[pygame.Rect]
    group : pyscroll.PyscrollGroup
    tmx_data: pytmx.TiledMap
    portals: list[Portal]
    npcs: list[NPC]
             
class MapManager:
    
    def __init__(self, screen,player):
        self.player = player
        self.screen = screen
        self.maps = dict() 
        self.current_map = "world"
        
        self.register_map("world", portals=[
            Portal(from_world="world", condition= -1, origin_point="enter_house", target_world="house", teleport_point="spawn_house"),
            Portal(from_world="world", condition= -1, origin_point="enter_dungeon", target_world="dungeon", teleport_point="spawn_dungeon"),
            Portal(from_world="world", condition= pygame.K_o, origin_point= None, target_world = "picNashor", teleport_point = "spawn_nashor")
        ], npcs=[
            NPC("garen",nb_points=1, dialog=["DEMACIA!"]),
            NPC("ahri",nb_points=1, dialog=["Fini de jouer"]),
            NPC("yi",nb_points=1, dialog=["Voulez vous entrer dans le pic du Baron Nashor? Oui: O, Non: N"],condition=pygame.K_o),
            NPC("lulu",nb_points=1, dialog=["Laissez passer !"]),
            NPC("ezreal",nb_points=1, dialog=["C'est dangereux ? J'en suis !"])
        ])
        self.register_map("house", portals=[
            Portal(from_world="house", condition= -1, origin_point="exit_house", target_world="world", teleport_point="enter_house_exit"),
            Portal(from_world="house", condition= -1, origin_point="enter_house2", target_world= "house2", teleport_point="spawn_house")
        ])
        
        self.register_map("picNashor", portals=[
            Portal(from_world="picNashor", condition= pygame.K_o, origin_point= None, target_world = "World", teleport_point = "spawn_exixt_nashor")],
            npcs=[
                NPC("yi", nb_points=1,dialog=["Prepare toi à perdre..."])])
        self.register_map("house2", portals=[
            Portal(from_world="house2", condition= -1,origin_point="exit_house", target_world="house", teleport_point="exit_house2")
        ])
        
        self.register_map("dungeon", portals=[
            Portal(from_world="dungeon", condition= -1,origin_point="exit_dungeon", target_world="world", teleport_point="dungeon_exit_spawn")
        ], npcs=[
            NPC("boss", nb_points=2, dialog=["blaba"])
        ])
        
        self.teleport_player("player")
        self.teleport_npcs()
        
        
        



            
    def check_npc_collisions(self, dialog_box, key):
        for sprite in self.get_group().sprites():
            if sprite.feet.colliderect(self.player.rect) and type(sprite) is NPC:
                if key == pygame.K_SPACE:
                    self.player.not_talking = False
                    if not(dialog_box.execute(sprite.dialog)):
                        self.player.not_talking= True
                if key == sprite.condition: 
                    dialog_box.execute(sprite.dialog)
                    self.player.not_talking = True
                    self.check_portals(sprite.condition)
            
            
    
    def check_portals(self,condition=-1):
        for portal in self.get_map().portals:
            if portal.from_world == self.current_map:
                if portal.origin_point != None:
                    point = self.get_object(portal.origin_point)
                    rect = pygame.Rect(point.x, point.y, point.width, point.height)
                copy_condition = portal.condition
                if copy_condition == -1:
                    copy_condition = self.player.feet.colliderect(rect) 
                elif copy_condition > 0:
                    if copy_condition == condition:
                        copy_condition = True
                    else : copy_condition = False
                if copy_condition:
                    copy_portal = portal
                    self.current_map = portal.target_world
                    self.teleport_player(copy_portal.teleport_point)
                
    def check_collisions(self):
        #portails
        
        #self.check_portals()           
        #collision
        for sprite in self.get_group().sprites():
            
            if type(sprite) is NPC:
                if sprite.feet.colliderect(self.player.rect):
                    sprite.speed = 0
                else: sprite.speed = 1
                
            if sprite.feet.collidelist(self.get_walls()) > -1:
                sprite.move_back()
        
        self.check_portals()          
          
    def teleport_player(self, name):
        point = self.get_object(name)
        self.player.position[0] = point.x
        self.player.position[1] = point.y
        self.player.save_location()
        
    def register_map(self, name, portals=[], npcs=[]):
         # charger la carte (tmx)
        tmx_data =  pytmx.util_pygame.load_pygame(f"map/{name}.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 3
        
        #definir une liste qui va stocker les rectangles de collision
        walls = []
        
        for obj in tmx_data.objects:
            if obj.type == "collision":
                walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))
            
        #dessiner le groupe de calques
        group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=4)
        group.add(self.player)
        #recuperer tout les npcs pour les ajouter au groupe
        for npc in npcs:
            group.add(npc)
            
        #creer un object Map
        self.maps[name] = Map(name,walls,group,tmx_data, portals, npcs)
    
    def get_map(self): return self.maps[self.current_map]
    
    def get_group(self): return self.get_map().group
    
    def get_walls(self): return self.get_map().walls
    
    def get_object(self, name): return self.get_map().tmx_data.get_object_by_name(name)
    
    def teleport_npcs(self):
        for map in self.maps:
            map_data = self.maps[map]
            npcs = map_data.npcs
            
            for npc in npcs:
                npc.load_points(map_data.tmx_data)
                npc.teleport_spawn()
                
    def draw(self):
        self.get_group().draw(self.screen)
        self.get_group().center(self.player.rect.center)
        Health_Bar = pygame.Rect(self.player.position[0],self.player.position[1],self.player.rect.width,self.player.rect.height)
        
        pygame.draw.rect(self.screen, (255,0,0), Health_Bar,2)
        
    def update(self):
        self.get_group().update()
        self.check_collisions()
       
        
        
        for npc in self.get_map().npcs:
            npc.move()