<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.2" name="collision" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="collision.png" trans="000000" width="128" height="128"/>
 <tile id="9">
  <objectgroup draworder="index" id="3">
   <object id="3" type="collision" x="0.0425306" y="0.0850611" width="15.949" height="15.8639"/>
  </objectgroup>
 </tile>
</tileset>
